/*
	YAPE - Yet Another Plus/4 Emulator

	The program emulates the Commodore 264 family of 8 bit microcomputers

	This program is free software, you are welcome to distribute it,
	and/or modify it under certain conditions. For more information,
	read 'Copying'.

	(c) 2000, 2001, 2004, 2005, 2007 Attila Gr�sz
	(c) 2005 VENESZ Roland
*/

#define TITLE   "YAPESDL 0.32"
#define NAME    "Yape for SDL 0.32.5"

#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <math.h>
#include <SDL/SDL.h>
#include "keyboard.h"
#include "cpu.h"
#include "tedmem.h"
#include "tape.h"
#include "sound.h"
#include "archdep.h"
#include "iec.h"
#include "device.h"
#include "tcbm.h"
#include "diskfs.h"
#include "prg.h"
#include "interface.h"

#ifdef __EMSCRIPTEN__
#include <emscripten.h>
#include <stdio.h>
#include <string.h>
#include <emscripten/html5.h>

#endif

// function prototypes
extern void FrameUpdate(void);

#include "interface.h"

// SDL stuff
static SDL_Event		event;
static SDL_Surface     *screen, *image;
static SDL_Palette		palette;
static SDL_Color		*g_pal;
static SDL_Rect			dest;

// class pointer for the user interface
static UI				*uinterface = NULL;

// color
static Uint8	p4col_calc[768];

///////////////////
// Screen variables
static Uint8	*pixel;

////////////////
// Supplementary
static Uint32			g_TotFrames = 0;
static Uint32			timeelapsed;

static TED				*ted8360 = new TED;
static CPU				*machine = new CPU(ted8360, &(ted8360->Ram[0xFF09]),
							&(ted8360->Ram[0x0100]));
static CTCBM			*tcbm = NULL;
static CIECInterface	*iec = NULL;
static CIECDrive		*fsdrive = NULL;
static Uint32			i;
static char				textout[40];
static char				inipath[512] = "";
static char				inifile[512] = "";

static Uint8			*screenptr;
static bool				g_bActive = true;
static bool				g_inDebug = false;
static bool				g_FrameRate = true;
static bool				g_50Hz = true;
static bool				g_bSaveSettings = true;
static bool				g_FullScreen = false;
static bool				g_SoundOn = true;
static bool				recording = false;
/* ---------- Inline functions ---------- */

inline double myMin(double a, double b)
{
    return a>b ? b : a;
}

inline double myMax(double a, double b)
{
    return a>b ? a : b;
}

//-----------------------------------------------------------------------------
// Name: ShowFrameRate()
//-----------------------------------------------------------------------------
inline static void ShowFrameRate()
{
	char fpstxt[80];
	sprintf( fpstxt, "%3i FPS", ad_get_fps());
	ted8360->texttoscreen(324,40,fpstxt);
}

//-----------------------------------------------------------------------------
// Name: DebugInfo()
//-----------------------------------------------------------------------------
inline static void DebugInfo()
{
	const Uint32 hpos = 36, vpos = 12;

	sprintf(textout,"OPCODE: %02x ",machine->getcins());
	ted8360->texttoscreen(hpos,vpos,textout);
	ted8360->texttoscreen(hpos,vpos+8,"  PC  SR AC XR YR SP");
	sprintf(textout,";%04x %02x %02x %02x %02x %02x",machine->getPC(),machine->getST(),machine->getAC(),machine->getX(),machine->getY(), machine->getSP());
	ted8360->texttoscreen(hpos,vpos+16,textout);
	sprintf(textout,"TAPE: %08d ",ted8360->tap->TapeSoFar);
	ted8360->texttoscreen(hpos,vpos+24,textout);
}

/* ---------- File loading functions ---------- */

void CopyToKbBuffer(char *text, unsigned int length)
{
	ted8360->Write(0xEF,length);
	while (length--)
		ted8360->Write(0x0527+length,text[length]);
}

//-----------------------------------------------------------------------------

void machineReset()
{
  ted8360->RAMenable = false;
  ted8360->ChangeMemBankSetup();
  ted8360->Reset();
  // this should not be HERE, but where else could I've put it??
  ted8360->tap->rewind();
  machine->Reset();
}

bool start_file(char *szFile )
{
	char fileext[4];

	if (szFile[0]!='\0') {
		for (i=0;i<4;++i) fileext[i]=tolower(szFile[strlen(szFile)-3+i]);
		if (!strncmp(fileext,"d64",3)) {
			/*if (!LoadD64Image(szFile)) {
				return false;
			}*/
			CopyToKbBuffer("L\317\042*\042,8,1\rRUN:\r", 15);
			return true;
		}
		if (!strcmp(fileext,"prg")) {
			PrgLoad( szFile, 0, (MemoryHandler*)ted8360 );
			CopyToKbBuffer("RUN:\r",5);
			return true;
		}
		if (!strcmp(fileext,"tap")) {
			ted8360->tap->detach_tap();
			strcpy(ted8360->tap->tapefilename, szFile);
			ted8360->tap->attach_tap();
			return true;
		}
		return false;
    }
	return false;
}

bool autostart_file(char *szFile)
{
	machineReset();
	// do some frames
	g_TotFrames = 0;
	while (g_TotFrames<20) {
		ted8360->ted_process();
		FrameUpdate();
		g_TotFrames++;
	}
	// and then try to load the parameter as file
	return start_file(szFile);
}

/* ---------- Display functions ---------- */

void init_palette()
{
	Uint32 colorindex;
	double	Uc, Vc, Yc,  PI = 3.14159265 ;
	double bsat = 45.0;

    /* Allocate 256 color palette */
    int ncolors = 256;
    g_pal  = (SDL_Color *)malloc(ncolors*sizeof(SDL_Color));

	// calculate palette based on the HUE values
	memset(p4col_calc, 0, 768);
	for ( i=1; i<16; i++)
		for ( register int j = 0; j<8; j++) {
		    Uint8 col;
			if (i == 1)
				Uc = Vc = 0;
			else {
				Uc = bsat * ((double) cos( HUE[i] * PI / 180.0 ));
				Vc = bsat * ((double) sin( HUE[i] * PI / 180.0 ));
			}
			Yc = (luma[j+1] - 2.0)* 255.0 / (5.0 - 2.0); // 5V is the base voltage
			// RED, GREEN and BLUE component
			colorindex = (j)*16*3 + i*3;
			col = (Uint8) myMax(myMin((Yc + 1.14 * Vc),255.0),0);
			p4col_calc[ colorindex ] = p4col_calc[ 384 + colorindex ] = col;
			col = (Uint8) myMax(myMin((Yc - 0.39465 * Uc - 0.58060 * Vc ),255.0),0);
			p4col_calc[ colorindex + 1] = p4col_calc[ 384 + colorindex + 1] = col;
			col = (Uint8) myMax(myMin((Yc + 2.029 * Uc),255.0),0);
			p4col_calc[ colorindex + 2] = p4col_calc[ 384 + colorindex + 2] = col;
		}

	for (i=0 ; i<256; ++i) {
		// Creating an 8 bit SDL_Color structure
		g_pal[i].b=p4col_calc[i*3+2];
		g_pal[i].g=p4col_calc[i*3+1];
		g_pal[i].r=p4col_calc[i*3];
		g_pal[i].unused=0;
	}

	palette.ncolors = 256;
	palette.colors = g_pal;

}
#define VIDEO_WIDTH 456
#define VIDEO_HEIGHT 312
uint8_t *oldpixels=NULL;
void blit8to32(SDL_Surface *source) {
//printf("Blitting 8 to 32\n");

	/*
	if(oldpixels==NULL)
		oldpixels=(uint8_t *)malloc(SCR_VSIZE*SCR_HSIZE*2);

	uint8_t *optr = oldpixels;
	uint8_t *sptr = (uint8_t *)source->pixels;

	while(*optr<*oldpixels+SCR_VSIZE*SCR_HSIZE*2){

	while(optr==sptr) {
		*optr++;
		*sptr++;
	}

	printf("[%x %d]",*optr,sptr);
		optr = sptr;

	}
*/
//return;
	uint8_t *videobuffer=(uint8_t *)source->pixels;
    if(screen!=NULL)
		if(SDL_MUSTLOCK(screen))
			SDL_LockSurface(screen);

    int row_bytes = screen->pitch / 8;
	uint8_t *output = (uint8_t*)screen->pixels;
//printf("blitting\n");
//printf("source %d %d\n",source->w,source->h);
    for ( uint16_t y = 0 ; y < VIDEO_HEIGHT; y++) {
            int yy = y*screen->w*4;
			int yx = y*VIDEO_WIDTH;
        for ( uint16_t x = 0 ; x < VIDEO_WIDTH ; x++)  {
			output[x+yy]=(uint8_t)videobuffer[x+yx];
          //  uint8_t pixel = videobuffer[x+yx];
           // uint8_t pixel2 = videobuffer[(x*4)+1+y*source->w];
           // uint8_t pixel3 = videobuffer[(x*4)+2+y*source->w];
           // uint8_t pixel4 = videobuffer[(x*4)+3+y*source->w];
            //if(pixel) {
//          pixel = rand()*100;//pixel&100;
          //  output[ x + yy ] = pixel;//(y*screen->pitch/8)) = 
           // output[ x*2 + 1 + yy ] = pixel;//(y*screen->pitch/8)) = 
            
            //*((uint32_t *) screen->pixels + x + yy) = pixel2;//rand()*255;
            
        }
    }
    SDL_UnlockSurface(screen);

}
char playback = 0;
char pressed =0;
inline void FrameUpdate(void)
{
    SDL_Rect rc;

    rc.x = (456-384)/2 - 4;
    rc.y = (312-288)/2;
    rc.w = 384;
    rc.h = 288;
	
//	SDL_BlitSurface( image, &rc , screen, NULL);
#ifdef __EMSCRIPTEN__
	blit8to32(image);
#else	
//	SDL_LockSurface(screen);
	SDL_BlitSurface( image, NULL , screen, NULL);
//SDL_SoftStretch(image, NULL,screen,NULL);
//	SDL_UnlockSurface(screen);
#endif
	SDL_Flip ( screen );
}

/* ---------- Management of settings ---------- */

//-----------------------------------------------------------------------------
// Name: SaveSettings
//-----------------------------------------------------------------------------
bool SaveSettings()
{
	FILE *ini;
	unsigned int rammask;

	if (ini=fopen(inifile,"wt")) {

		fprintf(ini, "[Yape configuration file]\n");
		fprintf(ini,"DisplayFrameRate = %d\n",g_FrameRate);
		fprintf(ini,"DisplayQuickDebugInfo = %d\n",g_inDebug);
		fprintf(ini,"50HzTimerActive = %d\n",g_50Hz);
		fprintf(ini,"JoystickEmulation = %d\n",ted8360->joyemu);
		fprintf(ini,"ActiveJoystick = %d\n",ted8360->keys->activejoy);
		rammask = ted8360->getRamMask();
		fprintf(ini,"RamMask = %x\n",rammask);
		fprintf(ini,"256KBRAM = %x\n",ted8360->bigram);
		fprintf(ini,"SaveSettingsOnExit = %x\n",g_bSaveSettings);

		for (i = 0; i<4; i++) {
			fprintf(ini,"ROMC%dLOW = %s\n",i, ted8360->romlopath[i]);
			fprintf(ini,"ROMC%dHIGH = %s\n",i, ted8360->romhighpath[i]);
		}

		fclose(ini);
		return true;
	}
	return false;
}

bool LoadSettings()
{
	FILE *ini;
	unsigned int rammask;
	char keyword[256], line[256], value[256];

	if (ini=fopen(inifile,"r")) {

		fscanf(ini,"%s configuration file]\n", keyword);
		if (strcmp(keyword, "[Yape"))
			return false;

		while(fgets(line, 255, ini)) {
			strcpy(value, "");
			if (sscanf(line, "%s = %s\n", keyword, value) ) {
				if (!strcmp(keyword, "DisplayFrameRate")) {
					g_FrameRate = !!atoi(value);
					//	fprintf( stderr, "Display frame rate: %i\n", g_FrameRate);
				} else if (!strcmp(keyword, "DisplayQuickDebugInfo"))
					g_inDebug = !!atoi(value);
				else if (!strcmp(keyword, "50HzTimerActive"))
					g_50Hz = !!atoi(value);
				else if (!strcmp(keyword, "JoystickEmulation"))
					ted8360->joyemu = !!atoi(value);
				else if (!strcmp(keyword, "ActiveJoystick"))
					ted8360->keys->activejoy = atoi(value);
				else if (!strcmp(keyword, "RamMask")) {
					sscanf( value, "%04x", &rammask);
					ted8360->setRamMask( rammask );
				}
				else if (!strcmp(keyword, "256KBRAM"))
					ted8360->bigram = !!atoi(value);
				else if (!strcmp(keyword, "SaveSettingsOnExit"))
					g_bSaveSettings = !!atoi(value);
				else if (!strcmp(keyword, "ROMC0LOW"))
					strcpy(ted8360->romlopath[0], value);
				else if (!strcmp(keyword, "ROMC1LOW"))
					strcpy(ted8360->romlopath[1], value);
				else if (!strcmp(keyword, "ROMC2LOW"))
					strcpy(ted8360->romlopath[2], value);
				else if (!strcmp(keyword, "ROMC3LOW"))
					strcpy(ted8360->romlopath[3], value);
				else if (!strcmp(keyword, "ROMC0HIGH"))
					strcpy(ted8360->romhighpath[0], value);
				else if (!strcmp(keyword, "ROMC1HIGH"))
					strcpy(ted8360->romhighpath[1], value);
				else if (!strcmp(keyword, "ROMC2HIGH"))
					strcpy(ted8360->romhighpath[2], value);
				else if (!strcmp(keyword, "ROMC3HIGH"))
					strcpy(ted8360->romhighpath[3], value);
			}
		}
		fclose(ini);

		if (!g_50Hz)
			g_SoundOn = false;

		return true;
	}
	return false;
}

inline void PopupMsg(char *msg)
{
	///printf("Message: %s\n",msg);
	Uint32 ix;
	size_t len = strlen(msg);
	char dummy[40];

	ix = Uint32(len);
	while( ix-->0)
		dummy[ix] = 32;
	dummy[len] = '\0';

	ted8360->texttoscreen(220-(len<<2),144,dummy);
	ted8360->texttoscreen(220-(len<<2),152,msg);
	ted8360->texttoscreen(220-(len<<2),160,dummy);
	FrameUpdate();
#ifndef __EMSCRIPTEN__
	SDL_Delay(750);
#endif
}

//-----------------------------------------------------------------------------
// Name: SaveBitmap()
// Desc: Saves the SDL surface to Windows bitmap file named as yapeXXXX.bmp
//-----------------------------------------------------------------------------
int SaveBitmap()
{
	bool			success = true;
    char			bmpname[16];
	FILE			*fp;
	int				ix = 0;

    // finding the last yapeXXXX.bmp image
    while (success) {
        sprintf( bmpname, "yape%.4d.bmp", ix);
        fp=fopen( bmpname,"rb");
        if (fp)
            fclose(fp);
		else
			success=false;
        ix++;
    };
	//fprintf( stderr, "%s\n", bmpname);
	return SDL_SaveBMP( screen, bmpname);
}
#define MAX_EVENTS 65535

struct port1_event {
  uint32_t cycle;
  uint16_t key;
  uint8_t val;
} events[MAX_EVENTS], *cevent = NULL, *lastevent = NULL;

char keys[1024];

int turns=1;
extern unsigned char sdltrans[2048];
//-----------------------------------------------------------------------------
// Name: poll_events()
// Desc: polls SDL events if there's any in the message queue
//-----------------------------------------------------------------------------
void poll_events(void)
{
	uint32_t cycle = ted8360->GetClockCount();
	event.type = NULL;
	event.key.keysym.sym = (SDLKey) 0;

if(playback) {
		if(!cevent->cycle) {
			fprintf(stderr,"%d %d playback ended, resuming control to player\n",cevent,events);
			playback = 0;
			recording = 1;
			turns=0;
		}
		
		// if cpu if equal or ahead of our timestamp for the input
		if(cycle >= cevent->cycle) {
			if(cevent->val==1) {
			//	printf("pushing event %x at (%x) key[%x]\n",cevent->cycle, cycle, cevent->key);
				ted8360->keys->pushkeyraw(cevent->key);
			}
			else {
				if(cevent->key) {
			//		printf("releasing event %x at (%x) key[%x]\n",cevent->cycle, cycle, cevent->key);
					ted8360->keys->releasekeyraw(cevent->key);
				}
			}
			pressed = 1;
			++cevent;
	
		}
	return;
}
//	if(!playback) 
		SDL_PollEvent(&event);

    if ( event.type ) {

		int key = sdltrans[event.key.keysym.sym];

        switch (event.type) {
	        case SDL_KEYDOWN:
			
//			if(!playback) {
//				if(keys[key]==0) 
//					
//					//event.key.keysym.scancode,
//                  //SDL_GetKeyName(event.key.keysym.sym));
//                keys[key] = 1;
//			}
			
				if (event.key.keysym.mod & KMOD_LALT) {
					sound_pause();
						switch (event.key.keysym.sym) {
							case SDLK_j :
								ted8360->joyemu=!ted8360->joyemu;
								if (ted8360->joyemu) {
									sprintf(textout , " JOYSTICK EMULATION IS ON ");
									ted8360->keys->joyinit();
								} else {
									sprintf(textout , " JOYSTICK EMULATION IS OFF ");
									ted8360->keys->releasejoy();
								};
								PopupMsg(textout);
								break;
							case SDLK_i :
								ted8360->keys->swapjoy();
								sprintf(textout, "ACTIVE JOY IS : %d", ted8360->keys->activejoy);
								PopupMsg(textout);
								break;
							case SDLK_w :
								g_50Hz = ! g_50Hz ;
								if (g_50Hz) {
									sound_resume();
									sprintf(textout , " 50 HZ TIMER IS ON ");
								} else {
									sound_pause();
									sprintf(textout , " 50 HZ TIMER IS OFF ");
								}
								PopupMsg(textout);
								// restart counter
								g_TotFrames = 0;
								timeelapsed = SDL_GetTicks();
								break;
							case SDLK_s:
								g_TotFrames = 0;
								g_FrameRate = !g_FrameRate;
								break;
							case SDLK_RETURN:
#ifndef WIN32
								SDL_WM_ToggleFullScreen( screen );
#else
								g_FullScreen = !g_FullScreen;
								{
									int bpp;
									Uint32 flags = SDL_HWPALETTE|
										SDL_SRCCOLORKEY|SDL_HWSURFACE|SDL_DOUBLEBUF;
									if (g_FullScreen) {
										bpp = 32;
										flags |= SDL_FULLSCREEN;
										SDL_ShowCursor(SDL_DISABLE);
									} else {
										bpp = 32;
										SDL_ShowCursor(SDL_ENABLE);
									}
									screen = SDL_SetVideoMode(456, 312, bpp, flags);
									delete uinterface;
									uinterface = new UI(ted8360, screen, image, inipath);
								}
#endif
								break;
						};
						if (g_50Hz) sound_resume();
						return;
				}
				switch (event.key.keysym.sym) {

					case SDLK_PAUSE :
						if (g_bActive)
							PopupMsg(" PAUSED ");
						g_bActive=!g_bActive;
						break;
					case SDLK_F5 :
					case SDLK_F6 :
						ted8360->tap->PressTapeButton(ted8360->GetClockCount());
						break;

						break;
					case SDLK_F7:
						if (!SaveBitmap( ))
							fprintf( stderr, "Screenshot saved.\n");
						break;
					case SDLK_ESCAPE:
//					case SDLK_TAB:
						sound_pause();
						uinterface->enterMenu();
						sound_resume();
						break;
					case SDLK_F9 :
						g_inDebug=!g_inDebug;
						break;
					case SDLK_F10 :
						if (SaveSettings())
						  fprintf( stderr, "Settings saved to %s.\n", inifile);
						else
						  fprintf( stderr, "Oops! Could not save settings... %s\n", inifile);
						break;
					case SDLK_F11 :
						g_bActive = false;
						break;
					case SDLK_F12 :
						// Save settings if required
						if (g_bSaveSettings)
							SaveSettings();
						close_audio();
						exit(0);
					default :
						
						if(recording) {
							if(keys[key]==0) {
								printf("%08x %3d 1\n",// The %d,%d,%s key was pressed!\n",
									cycle,
									key);//,
							
								keys[key]=1;
							
							}
						}
						ted8360->keys->pushkeyraw(key);
				}
				break;

	        case SDL_KEYUP:
/*	              printf("at %x The %d,%d,%s key was released!\n",
					ted8360->GetClockCount(),
					event.key.keysym.sym,
					event.key.keysym.scancode,
                   SDL_GetKeyName(event.key.keysym.sym)
                   );
*/
     		switch (event.key.keysym.sym) {
					case SDLK_F11 :
						g_bActive = true;
		                if (event.key.keysym.mod & (KMOD_LSHIFT|KMOD_RSHIFT) ) {
				  machineReset();
							break;
						}
						if (event.key.keysym.mod & (KMOD_LCTRL|KMOD_RCTRL) ) {
							ted8360->RAMenable = false;
							ted8360->ChangeMemBankSetup();
							break;
			            }
						machine->softreset();
						break;
					default:
						if(recording) {
							//if(keys[sdltrans[event.key.keysym.sym]]==1) {
								keys[key]=0;
								printf("%08x %3d 0\n",
									cycle,
									key
								);
						//	}
						}
						ted8360->keys->releasekeyraw(key);//&0x1FF
				}
				break;

            case SDL_QUIT:
				// close audio
				close_audio();
				// release the palette
				if (g_pal)
					free(g_pal);
				if (g_bSaveSettings)
					SaveSettings();
				SDL_Quit();
                exit(0);
        }
    }
}

static void MachineInit()
{
	CFakeTCBM *tcbm_l = new CFakeTCBM();
	CFakeIEC *iec_l = new CFakeIEC(8);
	fsdrive = new CIECFSDrive(".");

	iec = iec_l;
	tcbm = tcbm_l;
	tcbm_l->AddIECInterface((CIECInterface*)iec);
	iec_l->AddIECDevice((CIECDevice*)fsdrive);
	ted8360->loadroms();
	ted8360->HookTCBM(tcbm);
	tcbm_l->Reset();
	iec->Reset();
	fsdrive->Reset();
	CSerial::InitPorts();

	uinterface = new UI(ted8360, screen, image, inipath);
}

static void app_initialise()
{
	char		drvnamebuf[16];

    if ( SDL_Init(SDL_INIT_VIDEO | SDL_INIT_AUDIO | SDL_INIT_JOYSTICK) < 0 ) { //  SDL_INIT_AUDIO|
        fprintf(stderr, "Unable to init SDL: %s\n", SDL_GetError());
        printf("WHOOPS!\n");
        exit(1);
    }
	atexit(SDL_Quit);

	// check the video driver we have
	SDL_VideoDriverName( drvnamebuf, 16);
	fprintf(stderr, "Using video driver : %s\n", drvnamebuf);

	// set the window title and icon
	SDL_WM_SetCaption(NAME, "Yape");
	SDL_WM_SetIcon(SDL_LoadBMP("Yapelin.bmp"), NULL);

	// set the appropriate video mode
	screen = SDL_SetVideoMode(456, 312, 
#ifdef __EMSCRIPTEN__
	32,
#else
	8,
#endif
	
//	screen = SDL_SetVideoMode(384, 288, 32,
		/*SDL_RESIZABLE|SDL_HWPALETTE|*/SDL_SRCCOLORKEY|SDL_HWSURFACE|SDL_DOUBLEBUF  );
		// SDL_DOUBLEBUF|SDL_FULLSCREEN|SDL_RESIZABLE
	if ( screen == NULL) {
        fprintf(stderr, "Unable to set video mode\n");
        exit(1);
    }

	event.type=SDL_VIDEORESIZE;
	event.resize.w=384;
	event.resize.h=288;
	SDL_PushEvent(&event);

	// set the window range to be updated
    dest.x = 0;
    dest.y = 0;
    dest.w = screen->w;
    dest.h = screen->h;

	// calculate and initialise palette
	init_palette();

	// set colors to the screen buffer
	int SDLresult = SDL_SetColors(screen, g_pal, 0, 256);

//	SDL_LockSurface(screen);
	// set the backbuffer to the same format
#ifdef __EMSCRIPTEN__
	image = SDL_ConvertSurface(screen, screen->format, SDL_HWSURFACE);
#else

	image = SDL_DisplayFormat(screen);
#endif

//	image = SDL_CreateRGBSurface(0, screen->w, screen->h, 8,0,0,0,0);
/*
    for(i = 0; i < 256; i++) {
        sdl_palette[i] = sdl_local[col[i] & 0x0F];
        cols[i]=SDL_MapRGB(sdl_screen->format, 
					sdl_palette[i].r,
					sdl_palette[i].g,
					sdl_palette[i].b);
      //  printf("I%d R%d G%d B%d\n",i,sdl_palette[i].r,sdl_palette[i].g,sdl_palette[i].g); 
	}
*/
	//, screen->w, screen->h, screen->depth,0,0,0,0);
	//screen-> DisplayFormat(screen);

	// set the colors for the backbuffer too
	if (image->format->palette!= NULL ) {
        SDL_SetColors(screen,
                      image->format->palette->colors, 0,
                      image->format->palette->ncolors);
    }

	// change the pointer to the pixeldata of the backbuffer
	image->pixels = ted8360->screen;
//	screen->pixels = ted8360->screen;
//	SDL_UnlockSurface(screen);
	if (SDL_EnableKeyRepeat(0, SDL_DEFAULT_REPEAT_INTERVAL))
		fprintf(stderr,"Oops... could not set keyboard repeat rate.\n");

	init_audio();
}


#define TICK_INTERVAL    18

static Uint32 next_time;


Uint32 time_left(void)
{
    Uint32 now;

    now = SDL_GetTicks();
    if(next_time <= now)
        return 0;
    else
        return next_time - now;
}

/* ---------- MAIN ---------- */

void loop(void) {
		if(playback)
			turns=50;
while(turns--) {
		poll_events();
//	requestAnimationFrame(loop);
	   next_time = SDL_GetTicks();// + TICK_INTERVAL;
	// hook into the emulation loop if active
	if (g_bActive) {
		ted8360->ted_process();
//printf("Polling events\n");
		//poll_events();

		if (g_inDebug)
			DebugInfo();
	   next_time += TICK_INTERVAL;
#ifndef __EMSCRIPTEN__
if(!playback) {
		SDL_Delay(time_left());
		// frame update
		if (g_FrameRate)
			ShowFrameRate();
}
#else
// 
	//	while (SDL_GetTicks() < next_time) {
//			usleep(1);
//		}
#endif
//		ad_vsync(g_50Hz ); // && !isSoundOn()
// update the screen either at the end of vblank or when a key is pressed
// if playing back (super speed!)

		if((turns<1 || pressed)) {// && cevent>=lastevent) {// && (int)(cevent-events)<5) {
			//printf("%d\n",(int)(cevent - events));	
//			printf("Updating Frame\n");
			FrameUpdate();
		}
	} else {
#ifndef __EMSCRIPTEN__
if(!playback)
		SDL_WaitEvent(NULL);
#endif
	}
		if((playback && pressed) || turns<1) {
			pressed =0 ;
			return;
		}
	}
	
	
}
int main(int argc, char *argv[])
{
	//printf("running %s\n", __FUNCTION__);

	app_initialise();
	MachineInit();

	if (getenv("HOME")) {
		strcpy( inipath, getenv("HOME"));
		fprintf(stderr,"Home directory is %s\n",inipath);
		ad_makedirs( inipath );
	} else {
		ad_get_curr_dir( inipath );
		fprintf(stderr,"No home directory, using current directory: %s\n", inipath);
	}

	//printf("Here we go...\n");

	memset(keys,0,1024);
	
	strcpy( inifile, ad_getinifilename( inipath ) );
	fprintf(stderr,"Config file: %s\n", inifile);

	if (LoadSettings()) {
		fprintf(stderr,"Settings loaded successfully.\n");
	} else
		fprintf(stderr,"Error loading settings or no .ini file present...\n");
	if (!g_SoundOn) sound_pause();
	KEYS::initPcJoys();
	machine->Reset();
	ted8360->cpuptr = machine;
	/* ---------- Command line parameters ---------- */

    /* Replay mode */
    if(argc==3) {
    FILE *f = fopen(argv[2], "r");
    if (!f) {
      fprintf(stderr, "(non fatal) could not open replay file \"%s\"\n", argv[2]);

      //exit(1);
    } else {

    for ( cevent = events ; 3 == fscanf(f, "%x %hd %hd", &(cevent->cycle), &(cevent->key), &(cevent->val)) ; cevent++ )
      ;
    fclose(f);
    fprintf(stderr,"replaying %d events\n", cevent - events);
	lastevent = cevent;

    cevent->cycle = 0; /* Indicates end */ playback=1;
    recording=0;
    cevent = events;
    //argc--, argv++;
	}
}
		
	if (argv[1]!='\0') {
		//printf("Parameter 1 :%s\n", argv[1]);
		// and then try to load the parameter as file
		autostart_file(argv[1]);
		if(argc<3)
			recording=1;
	}
	 
else {	
  
// autostart_file("games/Fingers_Malone_Sys5792.prg");
//CopyToKbBuffer("SYS 5792:\r",10);
//autostart_file("games/icicle_works.prg");

//autostart_file("games/fingers_malone.prg");
}
	ad_vsync_init();
	/* ---------- MAIN LOOP ---------- */
	/* main game loop */

 
/*    while ( game_running ) {
        update_game_state();
        
    } */
    
	unsigned int timeelapsed = SDL_GetTicks();
#ifdef __EMSCRIPTEN__
	//printf("Handing control to javascript looper\n");

	emscripten_set_main_loop(loop,0,true);
#else 
		for (;;) {
			loop();
		}
#endif
	return 0;
}
