/*
	YAPE - Yet Another Plus/4 Emulator

	The program emulates the Commodore 264 family of 8 bit microcomputers

	This program is free software, you are welcome to distribute it,
	and/or modify it under certain conditions. For more information,
	read 'Copying'.

	(c) 2000, 2001, 2004, 2005, 2007 Attila Gr�z
*/

#include "sound.h"
#include "tedmem.h"
//#include <crtdbg.h>
#include <stdio.h>
#include <stdlib.h>
#include <memory.h>
#include <math.h>

#define PRECISION 4
#define OSCRELOADVAL (0x3FF << PRECISION)

static Sint32             Sound;

static Sint32             MixingFreq;
static Uint32             BufferLength;
static Sint32             Volume;
static Sint32             Snd1Status;
static Sint32             Snd2Status;
static Sint32             SndNoiseStatus;
static Sint32             DAStatus;
static Uint16  			  Freq1;
static Uint16  			  Freq2;
static Sint32             NoiseCounter;
static Sint32             FlipFlop[2];
static Sint32             oscCount1;
static Sint32             oscCount2;
static Sint32             OscReload[2];
static Sint32             oscStep;
static Sint8            noise[256]; // 0-8
static ClockCycle		lastUpdateCycle;
static Uint32			sndBufferPos;

static Sint32				write_position;
static Sint32				play_position;
static SDL_AudioSpec		*audiohwspec;

struct snd_frag {
	snd_frag *next;
	int valid;
	Sint16 *buf_data;
};

static snd_frag *first_frag;
static snd_frag *last_frag;
inline void render_audio(unsigned int nrsamples, Sint16 *buffer);

// adds a new frag to the end of the list
void add_new_frag(snd_frag *last_fragment, int fragsize)
{
    snd_frag *frag;
    
    frag = new snd_frag;
    if (last_fragment)
    	last_fragment->next = frag;
	else
		first_frag = last_frag = frag;
    frag->buf_data = (Sint16*) calloc(2*BufferLength, 1);
    frag->next = NULL;
    write_position++;
  
    last_frag = frag;
}

// deletes first frag in the list along with its buffer, 
// returns the next valid frag
void delete_frag(snd_frag *frag)
{
    snd_frag *new_first_frag = NULL;
    
    if (frag) {
		if (frag->next == NULL)
			last_frag = NULL;
		delete (frag->buf_data);
		new_first_frag = frag->next;
		delete frag;
		play_position++;
	}			
	first_frag = new_first_frag;
}

inline int getLeadInFrags()
{
	return (int) (write_position - play_position);
}

void fragment_done()
{
	int lead_in_frags = getLeadInFrags();
	
	//fprintf( stderr, "Lead in frags: %i\n", lead_in_frags);
	while (lead_in_frags<3) {
	    //fprintf( stderr, "Adding a frag.\n");
		// Finish pending buffer...
		/*if (last_frag) {
			//_ASSERT(sndBufferPos <= BufferLength);
			render_audio( BufferLength - sndBufferPos, last_frag->buf_data + sndBufferPos);
			sndBufferPos = 0;
			lead_in_frags++;
		}*/
		add_new_frag( last_frag, BufferLength);
		render_audio( BufferLength, last_frag->buf_data);
		lead_in_frags++;
	}
}
    
void audio_callback(void *userdata, Uint8 *stream, int len)
{
	snd_frag *playthis = first_frag;
	if (playthis) {
	    if (len > (int)(BufferLength*2)) len = BufferLength * 2;
    	memcpy( stream, playthis->buf_data, len);
    	//fprintf( stderr, "Playing/removing a frag in length %i.\n",len);
    	delete_frag(playthis);
		//fragment_done();
 	}
}

inline Uint32 getNrOfSamplesToGenerate(unsigned int clock)
{
	clock >>= 3; // 1778400.0 1773447.5
	//fprintf( stderr, "Sound: %i cycles/%f samples\n", clock, (double) clock * (double) MixingFreq / (1778400.0/8.0));
	// OK this should really be INT but I'm tired right now
	return (Uint32) ((double) clock * (double) MixingFreq / (1778400.0/8.0)); //  + 0.5
}

inline void updateAudio(unsigned int nrsamples)
{
	if (getLeadInFrags() > 6) {
		//fprintf( stderr, "Skipping a frag.\n");
		return;
	}

	if (!last_frag) {
		add_new_frag( 0, BufferLength);
		sndBufferPos = 0;
		//fragment_done();
		//return;
	}

	if (sndBufferPos + nrsamples >= BufferLength) {
		render_audio(BufferLength - sndBufferPos, last_frag->buf_data + sndBufferPos);
		fragment_done();
		add_new_frag(last_frag, BufferLength);
		if (sndBufferPos = (sndBufferPos + nrsamples) % BufferLength)
			render_audio(sndBufferPos, last_frag->buf_data);
	} else {
		render_audio(nrsamples, last_frag->buf_data + sndBufferPos);
		sndBufferPos += nrsamples;
	}
	//_ASSERT(sndBufferPos <= BufferLength);
}

void flushBuffer(ClockCycle cycle)
{
	unsigned int cyclesElapsed = (Sint32) (cycle - lastUpdateCycle);
	updateAudio(getNrOfSamplesToGenerate(cyclesElapsed));
	lastUpdateCycle = cycle;
}

inline void render_audio(unsigned int nrsamples, Sint16 *buffer)
{
    // Rendering...
	// Calculate the buffer...
	if (DAStatus) {// digi?
		short sample = audiohwspec->silence;
		if (Snd1Status) sample += Volume;
		if (Snd2Status) sample += Volume;
		for (;nrsamples--;) {
			*buffer++ = sample;
		}
	} else {
		Uint32 result1, result2;
		for (;nrsamples--;) {
			// Channel 1
			if ((oscCount1 += oscStep) >= OSCRELOADVAL) {
				if (OscReload[0] != (0x3FF << PRECISION)) 
					FlipFlop[0] ^= 1;
				oscCount1 = OscReload[0] + (oscCount1 - OSCRELOADVAL);
			}
			// Channel 2
			if ((oscCount2 += oscStep) >= OSCRELOADVAL) {
				if (OscReload[1] != (0x3FF << PRECISION)) {
					FlipFlop[1] ^= 1;
					if (NoiseCounter++==255) 
						NoiseCounter=0;
				}
				oscCount2 = OscReload[1] + (oscCount2 - OSCRELOADVAL);
			}
			result1 = (FlipFlop[0] && Snd1Status) ? Volume : 0;
			if (Snd2Status) {
				result2 = FlipFlop[1] ? Volume : 0;
			} else if (SndNoiseStatus) {
				result2 = noise[NoiseCounter] ? Volume : 0;
			} else {
				result2 = 0;
			}
			*buffer++ = audiohwspec->silence + result1 + result2;
		}   // for
	}
}

inline void setFreq(unsigned int channel, int freq)
{
//	if(channel==1)
//		freq = 0;
//	if(channel==0) {

	if (freq == 0x3FF) {
		freq = -1;
	} else if (freq == 0x3FE) {
		FlipFlop[channel] = 1;
	}
	OscReload[channel] = ((freq + 1)&0x3FF) << PRECISION;

//	printf("setFreq: %d %03d\n",channel, freq);

//	}
}

void writeSoundReg(ClockCycle cycle, Uint32 reg, Uint8 value)
{
	//printf("SoundReg: %X %X\n",reg, value);

	unsigned int cyclesElapsed = (Sint32) (cycle - lastUpdateCycle);
	updateAudio(getNrOfSamplesToGenerate(cyclesElapsed));

	switch (reg) {
		case 0:
			Freq1 = (Freq1 & 0x300) | value;
			setFreq(0, Freq1);
			break;
		case 1:
			Freq2 = (Freq2 & 0x300) | value;
			setFreq(1, Freq2);
			break;
		case 2:
			Freq2 = (Freq2 & 0xFF) | (value << 8);
			setFreq(1, Freq2);
			break;
		case 3:
			if (DAStatus = value & 0x80) {
				FlipFlop[0] = 1;
				FlipFlop[1] = 1;
				oscCount1 = OscReload[0];
				oscCount2 = OscReload[1];
				NoiseCounter = 0xFF;
			}
			Volume = value & 0x0F;
			if (Volume > 8) Volume = 8;
			Volume <<= 9;
			Snd1Status = value & 0x10;
			Snd2Status = value & 0x20;
			SndNoiseStatus = value & 0x40;
			break;
		case 4:
			Freq1 = (Freq1 & 0xFF) | (value << 8);
			setFreq(0, Freq1);
			break;
	}
	lastUpdateCycle = cycle;
}


void init_audio(unsigned int sampleFrq)
{
    SDL_AudioSpec *desired, *obtained = NULL;
    int i;

	MixingFreq = sampleFrq;//44100 22050 11025 96000 48000
	// Linux needs a buffer with a size of a factor of 2
	BufferLength = 1024;//2*FragmentSize 2048 4096 in Windows version...
	
	desired =(SDL_AudioSpec *) malloc(sizeof(SDL_AudioSpec));
	obtained=(SDL_AudioSpec *) malloc(sizeof(SDL_AudioSpec));

	desired->freq		= MixingFreq;
	desired->format		= AUDIO_S16;
	desired->channels	= 1;
	desired->samples	= BufferLength;
	desired->callback	= audio_callback;
	desired->userdata	= NULL;
	desired->size		= desired->channels * desired->samples * sizeof(Uint8);
	desired->silence	= 0x00;
	
	Sound = 0;
	sndBufferPos = 0;
	if (SDL_OpenAudio( desired, obtained)) {
		fprintf(stderr,"SDL_OpenAudio failed!\n");
		return;
	} else {
	    char drvnamebuf[16];
		fprintf(stderr,"SDL_OpenAudio success!\n");
    	SDL_AudioDriverName( drvnamebuf, 16);
        fprintf(stderr, "Using audio driver : %s\n", drvnamebuf);		
		if ( obtained == NULL ) {
			fprintf(stderr, "Great! We have our desired audio format!\n");
			audiohwspec = desired;
			free(obtained);
		} else {
			//fprintf(stderr, "Oops! Failed to get desired audio format!\n");
			audiohwspec = obtained;
			free(desired);
		}
	}
	MixingFreq = audiohwspec->freq;
	BufferLength = audiohwspec->samples;
	oscStep = (Sint32) (( 2.0 * 110860.45 * (double) (1 << PRECISION)) / (double) (MixingFreq));
    
	fprintf(stderr, "Obtained mixing frequency: %u\n",audiohwspec->freq);
	fprintf(stderr, "Obtained audio format: %04X\n",audiohwspec->format);
	fprintf(stderr, "Obtained channel number: %u\n",audiohwspec->channels);
	fprintf(stderr, "Obtained audio buffer size: %u\n",audiohwspec->size);
	fprintf(stderr, "Obtained sample buffer size: %u\n",audiohwspec->samples);
	fprintf(stderr, "Obtained silence value: %u\n",audiohwspec->silence);
	
	SDL_PauseAudio(0);
    FlipFlop[0] = 0;
    FlipFlop[1] = 0;
    oscCount1 = 0;
    oscCount2 = 0;
    NoiseCounter = 0;
	play_position = 0;
	write_position = 0;
	lastUpdateCycle = 0;
	Freq1 = Freq2 = 0;
	DAStatus = 0;

	/* initialise im with 0xa8 */
	int im = 0xa8;
    for (i=0; i<256; i++) {
		noise[i] = (im & 1) ? (64 / 9) : 0;
		im = (im<<1)+(1^((im>>7)&1)^((im>>5)&1)^((im>>4)&1)^((im>>1)&1));
    }

    Sound = 1;

    //SDL_PauseAudio(1);
}

void sound_pause()
{
	SDL_PauseAudio(1);
	Sound = 0;
}

int isSoundOn()
{
	return Sound;
}

void sound_resume()
{
	SDL_PauseAudio(0);
	Sound = 1;
}

void close_audio()
{
    SDL_PauseAudio(1);
#ifndef __EMSCRIPTEN__
    SDL_Delay(20);
#endif
	SDL_CloseAudio();
	if ( audiohwspec )
		free( audiohwspec );
}
