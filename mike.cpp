#include <dirent.h>
#include <stdio.h>
#include <malloc.h>
main(){

    struct dirent **namelist;

    int n;

    n = scandir(".", &namelist, 0, alphasort); 

    if (n < 0) printf("OUCH!\n");
//        perror("scandir"); 
    else { 

        while(n--) { 
            printf("%d - %s\n", n, namelist[n]->d_name); 
            free(namelist[n]); 
        } 

        free(namelist); 
    } 
}