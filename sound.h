#ifndef _SOUND_H
#define _SOUND_H

#include <SDL/SDL.h>
#include "types.h"

extern void init_audio(unsigned int sampleFrq = 48000);
extern void close_audio();
static void render_audio(unsigned int nrsamples, Sint16 *buffer);
extern void sound_pause();
extern void sound_resume();
extern int isSoundOn();

extern void flushBuffer(ClockCycle cycle);
extern void writeSoundReg(ClockCycle cycle, Uint32 reg, Uint8 value);

#endif
