objects =		\
archdep.o		\
cpu.o	\
diskfs.o \
dos.o \
drive.o \
iec.o \
interface.o		\
keyboard.o \
main.o \
prg.o \
serial.o	\
sound.o	\
tape.o \
tcbm.o \
tedmem.o \
#glob.o \
#bam.o			\
#diskimg.o \

headers = $(objects:.o=.h)
sources = $(objects:.o=.cpp)
allfiles = $(headers) $(sources)
hasnoheader = main.h dos.h
sourcefiles = $(filter-out $(hasnoheader),$(allfiles)) device.h mem.h roms.h types.h

CC = g++
cflags = -DC4DROID -O3 -finline -frerun-loop-opt -Winline -fomit-frame-pointer `sdl-config --cflags`
libs = `sdl-config --libs` -lm

#SDL_CFLAGS := $(shell sdl-config --cflags)
#SDL_LDFLAGS := $(shell sdl-config --libs)

yape : $(objects)
	$(CC) $(cflags) -o yape $(objects) $(libs) 

yapedebug : $(objects)
	$(CC) $(cflags) -g -o yape $^  $(libs)

archdep.o : archdep.cpp
	$(CC) $(cflags) -c $< 

#bam.o : bam.cpp bam.h
#	$(CC) $(cflags) -c $<

cpu.o : cpu.cpp tedmem.h
	$(CC) $(cflags) -c $<

diskfs.o : diskfs.cpp diskfs.h device.h iec.h
	$(CC) $(cflags) -c $<

#diskimg.o : diskimg.cpp diskimg.h
#	$(CC) $(cflags) -c $<

dos.o : dos.cpp
	$(CC) $(cflags) -c $<

drive.o : drive.cpp iec.cpp drive.h device.h diskfs.h iec.h tcbm.h
	$(CC) $(cflags) -c $<

iec.o : iec.cpp iec.h
	$(CC) $(cflags) -c $<

interface.o : interface.cpp
	$(CC) $(cflags) -c $< 

keyboard.o : keyboard.cpp keyboard.h
	$(CC) $(cflags) -c $< 

main.o : main.cpp
	$(CC) $(cflags) -c $<

prg.o : prg.cpp prg.h
	$(CC) $(cflags) -c $< 

sound.o : sound.cpp sound.h
	$(CC) $(cflags) -c $< 

tape.o : tape.cpp tape.h
	$(CC) $(cflags) -c $< 

tcbm.o : tcbm.cpp tcbm.h
	$(CC) $(cflags) -c $< 

tedmem.o : tedmem.cpp
	$(CC) $(cflags) -c $< 

clean :
	rm -f ./*.o

tgz :
	tar -czf yapeSDL_0.32.5.tar.gz $(sourcefiles) Makefile Yapelin.bmp COPYING README.SDL Changes

e :
	#emacs -fn 9x13 Makefile *.h *.cpp &
	emacs -fn 10x20 Makefile *.h *.cpp &

install : 
	@if [ ! -e $(HOME)/yape ]; then mkdir $(HOME)/yape ; fi
	@cp yape.conf $(HOME)/yape $^
	@cp ./yape $(BINDIR)
